import { useState} from "react";
import { Link } from 'react-router-dom'
import head from '../../assets/bg1.png'
import { useFetch } from "../../hooks/useFetch";

//styles
import './MoviesList.css'

export default function MoviesList() {
  const { data, isPending, error } = useFetch('http://localhost:3000/movies')

  const { status, setStatus} = useState('')

  // Delete function api
    const handleClick = (id) => {
      try{
      fetch('http://localhost:3000/movies/'+ id, 
            {method: 'DELETE'})
           .then(() => setStatus('Delete successful'));
      }
           catch(err ) {
            console.log(err)
    }
  }
 
    return (
      <div className="wrapper">
        {error && <p className="error">{error}</p>}
        { isPending && <div>Loading....</div>}
        {status && <p className="error">{status}</p>}
          <div className="movie-list"> 
                {data && data.map((movie) => (
                    <div className="card" key={movie.id}>
                    <div className="card-header">
                      <img src={head} alt="header"></img>
                    </div>
                      <h3 className=".card-title">{movie.title}</h3>
                        <div className="info">
                            <p>Type: {movie.type}</p>
                            <p>Genre: {movie.genre}</p>
                            <p>Views: {movie.views}</p>
                        </div>
                      <button className='btn' ><Link className="link"to ={`/movies/${movie.id}`}>Rent Movie</Link></button> 
                    <div className='icon'> <span class="material-icons-outlined" onClick={()=> handleClick(movie.id)}>
                </span></div>
              </div>
              )
            )}
          </div>
          </div>
    )
}

