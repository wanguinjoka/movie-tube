
import './Navbar.css'
import { Link } from 'react-router-dom'

import React from 'react';
import { useUserContext } from '../../context/userContext';


export default function Navbar() {
   const { user, logout } = useUserContext();

  return <div className='navbar' >
      <nav>
          <Link to="/" className='brand'><h1>Movie-Tube</h1></Link>
          
          <div className="profile">
            
          <h3>Welcome, {user.name}</h3>
          {!user.isGuestUser && (
            <button className="logout" onClick={logout}>
              LogOut
            </button>)}

            {user.isAdmin && (
              <button className='btn' ><Link className="link" to ={`/create`}>Create</Link></button> 
           )}
          
        </div>
      </nav>
  </div>;
}



