import { useState, useEffect} from 'react'
import { useFetch } from '../../hooks/useFetch'
import { useHistory } from "react-router-dom";

import './Create.css'

export default function Create() {
 
    const [title, setTitle] = useState('')
    const [genre, setMovieGenre] = useState('')
    const[ type, setMovieType] = useState('')
    const[ caption, setCaption] = useState('')
    const [views, setViews] = useState('')


    const history = useHistory();
  
    const { postData, data, error } = useFetch('http://localhost:3000/movies', 'POST')
     
    const handleSubmit = (e) => {
        e.preventDefault()
         postData({title, genre, type, caption, views})
    }
    useEffect(() => {
      if(data){
        history.push("/");
      }
    }, [data , history])
    

  return (
    <div className="create">
      {error && <p className="error">{error}</p>}
        <h2>Create Form</h2>
    <h2 className='page-title'>Add a New Movie:</h2>
    <form  onSubmit={handleSubmit}>
      <label>
        <span>Movie title:</span>
          <input 
            type='text'
            onChange={(e) => setTitle(e.target.value)}
            value ={title}
            required
          ></input>
      </label>
       <label>
         <span>Movie Type:</span>
         <div className='types'>
              <select required value={ type } onChange={(e)=> setMovieType(e.target.value)}>
                <option value="">--Please choose an option--</option>
                    <option value="regular">Regular</option>
                    <option value="children">Children</option>
                    <option value="newrealease">New Release</option>
                </select>
         </div>
       </label>
       <label>
         <span>Movie Genre:</span>
         <div className='genre'>
                <select required value={ genre } onChange={(e)=> setMovieGenre(e.target.value)}>
                  <option value="">--Please choose an option--</option>
                    <option value="action">Action</option>
                    <option value="drama">Drama</option>
                    <option value="romance">Romance</option>
                    <option value="comedy">Comedy</option>
                    <option value="horror">Horror</option>
                </select>
         </div>
       </label>
       <label>
        <span>Caption:</span>
        <input 
           type='text'
           maxlength="50"
           onChange={(e) => setCaption(e.target.value)}
           value ={caption}
           required
        ></input>
      </label>
      <label>
        <span>Views:</span>
        <input 
           type='number'
           maxlength="50"
           onChange={(e) => setViews(e.target.value)}
           value ={views}
           required
        ></input>
      </label>
      <button>Submit</button>
    </form>
  </div>
  )
}
