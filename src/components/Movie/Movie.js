import React,{ useState } from 'react'
import { useParams } from "react-router-dom"
import { useFetch } from '../../hooks/useFetch'
import head from '../../assets/bg1.png'

// imported the date range picker package

import { DateRange } from 'react-date-range'
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file

//styles
import './Movie.css'


export default function Movie() {
    const [rentTime, setRentTime] = useState('')
    const [price, setPrice] = useState('')
    const { id } = useParams()
    const url = 'http://localhost:3000/movies/'+ id
    const{ error, isPending, data: movie} = useFetch(url)
    const [day, setDay] = useState([
        {
          startDate: new Date(),
          endDate: null,
          key: 'selection'
        }
      ]);
 
//  The rate calculation function 
// to implement date range into func 
      const handleAdd =(e) =>{
          e.preventDefault();
            switch (movie.type) {
                case "regular":
                    setPrice( parseInt(rentTime)*1);
                break;
                case "children":
                    setPrice(parseInt(rentTime)*0.54);
                break;
                case "NewRelease":
                    setPrice(parseInt(rentTime)*1.5);
                break;
                default:
                    setPrice('minimum rate is 0.5');
                break;
            }
      }
  return (
        <div className="singleMovie">
            <div className='scard'>
                { error && <div>{error}</div>}
                { isPending && <div>Loading....</div>}
                { movie && (
                    <div className='scard1'>
                        <img className="header-img" src={head} alt="header"></img>
                            <h2>{movie.title}</h2>
                                <div>
                                <p>Type: {movie.type}</p>
                                <p>Genre: {movie.genre}</p>
                                <p>Views: {movie.views}</p>
                                </div>
                            <h3>Caption</h3>
                        <p>{movie.caption}</p>
                    </div>
                    )}
                    </div>

       {/* To refactor and extract to its own component */}
            <div className='calculate'>
                
                <h3>Payments</h3>
                <p>Days when Movie is avaialble</p>
                    <DateRange
                            editableDateInputs={true}
                            onChange={item => setDay([item.selection])}
                            moveRangeOnFirstSelection={false}
                            ranges={day}
                         />
                        <form>
                            <label>
                                <span>How Many Days:</span>
                                    <input 
                                        type='number'
                                        minlength="1" maxlength="8"
                                        onChange={e =>setRentTime(e.target.value)}
                                        value={rentTime}
                                        required
                                        />
                    </label>
                    <button onClick={handleAdd} className='cal-btn'>Calculate</button>
                    </form>
                    <p>Total Price: {price} $</p>
                </div>
    </div>
  )
}
