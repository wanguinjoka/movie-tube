import React, { useState } from 'react'
import { useUserContext } from '../../context/userContext';

// styles
import './Login.css'

export default function LoginForm() {

  const [username, setUserName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

// use the useContext to store username globally
  const { login } = useUserContext();

// form validation to add to it further
  function validateForm() {
    return email.length > 0 && password.length > 0   
  }

  function handleSubmit(e) {
    e.preventDefault();
    login(username);
  }

  return (
    <div className="login">
      <form onSubmit={handleSubmit}>
        <h1>Login</h1>
        <label>
           <span>Username</span>
                <input 
                  type='text'
                  name="username"
                  onChange={(e) => setUserName(e.target.value)}
                  value ={username}
                  required
                ></input>
            </label>
      <label>
        <span>email</span>
            <input 
              type='email'
              name="email"
              onChange={(e) => setEmail(e.target.value)}
              value ={email}
              required
            ></input>
      </label>
      <label>
        <span>password</span>
            <input 
              type='password'
              name="password"
              onChange={(e) => setPassword(e.target.value)}
              value ={password}
              required
            ></input>
      </label>
      <button className="login" disabled={!validateForm()}>Submit</button>
      </form>
    </div>
  );
};


