import React from "react";
import {Route, Switch } from 'react-router-dom'
import Login from "../Login/Login";
import MoviesList from "../Home/MoviesList";
import Movie from '../Movie/Movie'
import Create from '../create/Create'
import { useUserContext } from "../../context/userContext";

export default function Auth() {
  const  { user }  =  useUserContext();

  if(user.isGuestUser  && !user.isAdmin){
    return (
      <Login />)
  }else if(!user.isGuestUser  && !user.isAdmin){
    return (
      <>
      <Switch>
       <Route exact path="/">
           < MoviesList />
            </Route>      
            <Route exact path="/movies/:id">
                <Movie />
            </Route>
            <Route exact path="/create">
              {/* Handling when notadmin user trying to access could be improved */}
               { <h2> Cannot access page User Must be admin</h2>}
            </Route>
        </Switch> 
    </> 
    )
  }else if(!user.isGuestUser  && user.isAdmin){
    return (
      <>
      <Switch>
       <Route exact path="/">
          < MoviesList />
            </Route>      
            <Route exact path="/movies/:id">
                <Movie />
            </Route>
            <Route exact path="/create">
                <Create />
            </Route>
        </Switch> 
    </> 
    )
  }
}
  