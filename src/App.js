
import './App.css';
import { BrowserRouter } from 'react-router-dom'

import Navbar from './components/Navbar/Navbar';
import Auth from './components/Auth/Auth';



import { UserContextProvider } from './context/userContext';

function App() {
  
  return (
    <div className="App">
      <UserContextProvider>
          <BrowserRouter>
              <Navbar />
             <Auth/>
          </BrowserRouter>
      </UserContextProvider>
    </div>
  );
}

export default App;
