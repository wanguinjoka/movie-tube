import { createContext, useContext, useState } from "react";
import { useHistory } from "react-router-dom";


// creating the usercontext
export const userContext = createContext({
    user: null,
    login:()=> {},
    logout:()=>{},
})

// initial state
const USER = { name:"Guest", isGuestUser: true, isAdmin: false };

export function UserContextProvider({ children }) {
    const [user, setUser] = useState(USER);
    const history = useHistory();


    // define context login func
    function login(username){
        if(username === "admin"){
            setUser({name: username, isGuestUser: false, isAdmin: true})
        }else{
        setUser({ name: username, isGuestUser: false, isAdmin: false })
        }
    }

    function logout(){
        setUser(USER);
        history.push("/login");
    }

    return(
        <userContext.Provider value={{ user, login, logout }}>
            { children }
        </userContext.Provider>
    )
}

// useContext hook
export function useUserContext(){
    const { user, login, logout } = useContext(userContext);

    return { user, login, logout};
}